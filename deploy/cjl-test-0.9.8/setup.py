# -*- coding: utf-8 -*-

from setuptools import setup

setup(
    name='cjl-test',
    version='0.9.8',
    author='Chengjie Li',
    author_email='475168571@qq.com',
    description="some models and utils for study",
    license='HUST',
    packages=['cjltest'],
    install_requires=['torch', 'torchvision']
)
