FROM centos:centos7


COPY deploy/.condarc /root/

COPY deploy/Miniconda3-latest-Linux-x86_64.sh /tmp/

RUN bash /tmp/Miniconda3-latest-Linux-x86_64.sh -bfp /usr/local/ \
    && rm -rf /tmp/Miniconda3-latest-Linux-x86_64.sh \
    && conda install -y python=3.6 \
    && conda update conda \
    && conda clean --all --yes

RUN yum install net-tools && \
    yum install git && \
    yum install openssh-server -y
RUN conda install pytorch torchvision cpuonly -c pytorch
RUN pip install cjl-test


# RUN conda update -n base -c defaults conda
# COPY ./requirements.txt /home/admin/$APP_NAME/conf/
# COPY ./*.py /home/admin/$APP_NAME/
# RUN pip3 install torch==1.5.1+cpu torchvision==0.6.1+cpu -f https://download.pytorch.org/whl/torch_stable.html
# RUN pip3 install cjl-test
# COPY ./.condarc /root/
# RUN conda create -n python35 python=3.5
# RUN conda init bash
# RUN activate python35
# RUN pip install -r "/home/admin/$APP_NAME/conf/requirements.txt" -i "http://pypi.tuna.tsinghua.edu.cn/simple" --trusted-host "pypi.tuna.tsinghua.edu.cn"
# RUN pip install torch==1.4+cpu torchvision==0.5.0+cpu -f https://download.pytorch.org/whl/torch_stable.html
# RUN pip install torch==1.4+cpu torchvision==0.5.0+cpu
# RUN conda install pytorch torchvision cpuonly -c pytorch
# RUN pip install cjl-test


WORKDIR /home/admin/

ENTRYPOINT ["/bin/bash"]