# -*- coding: utf-8 -*-
import argparse
import os
import random
import math
import sys
import time
import socket
import datetime
import threading
import io

import torch
import torch.distributed as dist

parser = argparse.ArgumentParser()

parser.add_argument('--data-dir', type=str, default='~/dataset/')
parser.add_argument('--data-name', type=str, default='cifar10')
parser.add_argument('--model', type=str, default='MnistCNN')
parser.add_argument('--download', type=bool, default=True)
parser.add_argument('--N', type=int, default=20, help='update epochs at server')
parser.add_argument('--inner_epoch', type=int, default=4, help='K step at each worker')
parser.add_argument('--world_size', type=int, help="world_size, start at 0")
parser.add_argument('--world_rank', type=int, help="world rank, start at 0")
parser.add_argument('--master_addr', type=str, help="server addr")
parser.add_argument('--master_port', type=int, default=10086, help="server port")
parser.add_argument('--http_port', type=int, default=9999, help="http port")
parser.add_argument('--test_batch_size', type=int, default=400, help="http port")
parser.add_argument('--backend', type=str, default='gloo', help="http port")

lock = threading.Lock()

shapes = [
    (1, 2),
    (3, 4),
    (5, 6),
]


def receiver():
    local_params = []
    for i in range(3):
        tensor = torch.zeros(shapes[i])
        # tensor = torch.cat([tensor, tensor], dim=0)
        local_params.append(tensor)
    print(f"local param list: {len(local_params)}")

    while True:
        for idx in range(len(local_params)):
            dist.broadcast(local_params[idx], src=0)
            print(f"recv {local_params[idx]} \n", )


if __name__ == '__main__':
    args = parser.parse_args()
    # 进程总数
    world_size = args.world_size
    # 编号
    world_rank = args.world_rank
    hostname = socket.gethostname()
    server_addr_port = f'tcp://{args.master_addr}:{args.master_port}'
    print("init_processes with  ", server_addr_port)
    os.environ['MASTER_ADDR'] = args.master_addr
    os.environ['MASTER_PORT'] = str(args.master_port)
    dist.init_process_group(backend=args.backend,
                            init_method=server_addr_port,
                            timeout=datetime.timedelta(seconds=20),
                            rank=world_rank,
                            world_size=world_size)
    p = threading.Thread(target=receiver, args=())
    p.start()

    for i in range(3):
        tensor = torch.zeros(shapes[i])
        tensor += i
        buffer = io.BytesIO()
        torch.save(tensor, buffer)
        model_str = buffer.read().decode('UTF-8')
        print("model str: ", model_str)

        dist.send(tensor, dst=0)
        print(f"send {tensor} \n")
