#!/usr/bin/env python
import argparse
import torch
import os
import time
import torch.distributed as dist

parser = argparse.ArgumentParser()
parser.add_argument('--world_size', type=int, help="world_size, start at 0")
parser.add_argument('--world_rank', type=int, help="world rank, start at 0")
parser.add_argument('--master_addr', type=str, help="server addr")
parser.add_argument('--master_port', type=int, default=10086, help="server port")

def run(rank, size):
    time.sleep(2)
    tensor = torch.tensor([10])
    if rank == 0:  
        for i in range(1, size):
            for j in range(10):
                tensor += 1
                dist.send(tensor=tensor, dst=i)
    else:
        print("begin to recv data")
        for j in range(10):
            dist.recv(tensor=tensor, src=0)
            print('Rank ', rank, ' has data ', tensor[0])


def init_process(world_rank, world_size, fn, backend='gloo'):
    server_addr_port = f'tcp://{args.master_addr}:{args.master_port}'
    os.environ['MASTER_ADDR'] = args.master_addr
    os.environ['MASTER_PORT'] = str(args.master_port)
    dist.init_process_group(backend=backend, init_method=server_addr_port, rank=world_rank,world_size=world_size)
    fn(world_rank, world_size)


if __name__ == "__main__":
    args = parser.parse_args()

    init_process(world_rank=args.world_rank, world_size=args.world_size, fn=run)
