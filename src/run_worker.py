# -*- coding: utf-8 -*-
import argparse
import os
import time
import socket
import requests

import io
import datetime
import threading
import torch
import traceback

import torch.distributed as dist
import torch.distributed.rpc as rpc
from cjltest.divide_data import partition_dataset, select_dataset
from cjltest.models import MnistCNN, AlexNetForCIFAR
from cjltest.utils_data import get_data_transform
from torch.multiprocessing import Process as TorchProcess
from torch.utils.data import DataLoader
from cjltest.utils_model import MySGD
from torch.autograd import Variable
from torchvision import datasets, transforms
from utils import get_model_datasets, compress_nparr, broadcast_receive_model
from logging_tool import logger

parser = argparse.ArgumentParser()

parser.add_argument('--data_dir', type=str, default='~/dataset/')
parser.add_argument('--data_name', type=str, default='cifar10')
parser.add_argument('--model', type=str, default='LROnMnist')
parser.add_argument('--download', type=bool, default=True)
parser.add_argument('--N', type=int, default=20, help='update epochs at server')
parser.add_argument('--inner_epoch', type=int, default=4, help='K step at each worker')
parser.add_argument('--world_size', type=int, help="world_size, start at 0")
parser.add_argument('--world_rank', type=int, help="world rank, start at 0")

parser.add_argument('--master_addr', type=str, default="", help="server addr")

parser.add_argument('--master_port', type=int, default=10086, help="server port")
parser.add_argument('--http_addr', type=str, default='0.0.0.0', help="http port")
parser.add_argument('--http_port', type=int, default=9999, help="http port")
parser.add_argument('--backend', type=str, default='gloo', help="http port")

stop = False
lock = threading.Lock()
receive_param_pipes = []
global_model = None


def receiver():
    global stop, lock, global_model

    import udp_filetransfer
    import pickle
    while not stop:

        try:
            output = udp_filetransfer.receive()
            global_model = pickle.load(open(output, "rb"))
            # model = broadcast_receive_model()
            # lock.acquire()
            # global_model = model
        except Exception as e:
            traceback.print_exc()
            print(str(e))
        # global_model.load_state_dict(torch.load(output_file))

        # lock.release()


def run_worker(rank, workers, train_data, test_data):
    global global_model
    model = global_model
    for param in model.parameters():
        param.data = torch.zeros_like(param.data)

    if args.model in ['VGG', 'MnistCNN', 'AlexNet', 'ResNet18OnCifar10']:
        optimizer = MySGD(model.parameters(), lr=0.1)
    else:
        optimizer = MySGD(model.parameters(), lr=0.01)

    if args.model in ['MnistCNN', 'AlexNet']:
        criterion = torch.nn.NLLLoss()
    else:
        criterion = torch.nn.CrossEntropyLoss()

    model.train()

    global stop, rec, receive_param_pipes, lock
    p = threading.Thread(target=receiver, args=())
    p.start()


    WORKER_NUM = len(workers)
    INNER_EPOCH = args.inner_epoch

    computing_time_sum = 0.
    synchronization_time_sum = 0.

    accum_delta_w = []
    for _, param in enumerate(model.parameters()):
        accum_delta_w.append(torch.zeros_like(param.data))

    batch_iter = iter(train_data)
    next_upload_iter = rank * INNER_EPOCH
    n = 0
    base_http_url = f"http://{args.http_addr}:{args.http_port}"

    stime = time.time()

    while True:

        n += 1
        iteration_start_time = time.time()

        try:
            data, target = next(batch_iter)
        except Exception as e:
            logger.error(str(e))
            batch_iter = iter(train_data)
            data, target = next(batch_iter)

        # data, target = Variable(data), Variable(target)
        # target = torch.tensor(target)

        optimizer.zero_grad()
        output = model(data)
        loss = criterion(output, target)
        loss.backward()
        delta_ws = optimizer.get_delta_w()

        for idx, param in enumerate(model.parameters()):
            delta_ws[idx] /= WORKER_NUM
            param.data -= delta_ws[idx]
            accum_delta_w[idx] += delta_ws[idx]

        it_computing_time = time.time()

        if n >= next_upload_iter:

            logger.info(f"send {rank}, {time.time() - stime}, {n}")

            for idx, param in enumerate(model.parameters()):
                # tensor = torch.tensor(accum_delta_w[idx], device="cpu")
                tensor = accum_delta_w[idx].clone().detach()
                compressed_data, _, _ = compress_nparr(tensor.numpy())
                url = f"{base_http_url}/upload/{args.world_rank}/{idx}"
                resp = requests.post(url, data=compressed_data, headers={'Content-Type': 'application/octet-stream'})

                logger.info(f"send model {idx}, {tensor.shape}, {tensor.dtype}, {resp.text}")
                accum_delta_w[idx] = torch.zeros_like(param.data)
            update_url = f"{base_http_url}/update/{args.world_rank}"
            resp = requests.get(update_url)
            logger.info(f"trigger global model update {n}, {resp}, {time.time() - stime}")
            next_upload_iter += INNER_EPOCH * WORKER_NUM


        it_synchronization_time = time.time()
        computing_time_sum += it_computing_time - iteration_start_time
        synchronization_time_sum += it_synchronization_time - it_computing_time


def start():
    global global_model
    # 进程总数
    world_size = args.world_size
    # 编号
    world_rank = args.world_rank
    hostname = socket.gethostname()

    logger.info("World_rank:{}, hostname:{}".format(world_rank, hostname))

    workers = [v for v in range(1, world_size)]

    logger.info(f"workers: {workers}")
    model_datasets = get_model_datasets(model_name=args.model, data_dir=args.data_dir, download=args.download)
    global_model = model_datasets["model"]
    train_transform = model_datasets["train_transform"]
    test_transform = model_datasets["test_transform"]
    train_dataset = model_datasets["train_dataset"]
    test_dataset = model_datasets["test_dataset"]

    train_bsz = 400
    test_bsz = 400

    train_data = partition_dataset(train_dataset, workers)

    train_data = select_dataset(workers, world_rank, train_data, batch_size=train_bsz)

    test_data = DataLoader(test_dataset, batch_size=test_bsz, shuffle=True)

    if world_rank == 0:
        raise Exception("incorrect rank! ")

    server_addr_port = f'tcp://{args.master_addr}:{args.master_port}'
    logger.info(f"init_processes with  {server_addr_port}")
    os.environ['MASTER_ADDR'] = args.master_addr
    os.environ['MASTER_PORT'] = str(args.master_port)
    os.environ['GLOO_SOCKET_IFNAME'] = 'lo0'

    p = TorchProcess(target=run_worker, args=(world_rank, workers, train_data, test_data))

    p.start()
    p.join()


if __name__ == '__main__':
    args = parser.parse_args()
    start()
