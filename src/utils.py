
import io
import os
import string
import random
from tempfile import mkdtemp

import torch
import pickle
import traceback
import zlib
import time
import numpy as np

from torchvision import datasets, transforms
from torch.autograd import Variable
from cjltest.divide_data import partition_dataset, select_dataset
from cjltest.models import MnistCNN, AlexNetForCIFAR
from cjltest.utils_data import get_data_transform
from logging_tool import logger

import ResNetOnCifar10
from udp_receiver import ChunkReceiver
from udp_sender import ChunkSender


def get_model_datasets(model_name="MnistCNN", data_dir="/home/admin/data/", download=False):
    if model_name == 'MnistCNN':
        train_transform, test_transform = get_data_transform('mnist')
        model = MnistCNN()
        train_dataset = datasets.MNIST(data_dir, train=True, download=download,
                                       transform=train_transform)
        test_dataset = datasets.MNIST(data_dir, train=False, download=download,
                                      transform=test_transform)

    elif model_name == 'AlexNet':
        train_transform, test_transform = get_data_transform('cifar')
        model = AlexNetForCIFAR()
        train_dataset = datasets.CIFAR10(data_dir, train=True, download=download,
                                         transform=train_transform)
        test_dataset = datasets.CIFAR10(data_dir, train=False, download=download,
                                        transform=test_transform)

    elif model_name == 'LROnMnist':
        model = ResNetOnCifar10.LROnMnist()
        train_transform, test_transform = get_data_transform('mnist')
        train_dataset = datasets.MNIST(data_dir, train=True, download=download,
                                       transform=train_transform)
        test_dataset = datasets.MNIST(data_dir, train=False, download=download,
                                      transform=test_transform)

    elif model_name == 'LROnCifar10':
        model = ResNetOnCifar10.LROnCifar10()
        train_transform, test_transform = get_data_transform('cifar')
        train_dataset = datasets.CIFAR10(data_dir, train=True, download=download,
                                         transform=train_transform)
        test_dataset = datasets.CIFAR10(data_dir, train=False, download=download,
                                        transform=test_transform)

    elif model_name == 'ResNet18OnCifar10':
        model = ResNetOnCifar10.ResNet18()

        train_transform, test_transform = get_data_transform('cifar')
        train_dataset = datasets.CIFAR10(data_dir, train=True, download=download,
                                         transform=train_transform)
        test_dataset = datasets.CIFAR10(data_dir, train=False, download=download,
                                        transform=test_transform)
    else:
        raise Exception("incorrect model name")

    return {
        "model": model,
        "train_transform": train_transform,
        "test_transform": test_transform,
        "train_dataset": train_dataset,
        "test_dataset": test_dataset
    }


def test_model(model, test_data, criterion):
    test_loss = 0
    correct = 0
    model.eval()

    for data, target in test_data:
        data, target = Variable(data), Variable(target)
        output = model(data)
        test_loss += criterion(output, target).data.item()
        pred = output.data.max(1)[1]
        correct += pred.eq(target.data).sum().item()

    test_loss /= len(test_data)
    test_loss = format(test_loss, '.4f')
    acc = torch.tensor(correct / len(test_data.dataset))

    model.train()

    return test_loss, acc


def compress_nparr(nparr):
    """
    Returns the given numpy array as compressed bytestring,
    the uncompressed and the compressed byte size.
    """
    bytestream = io.BytesIO()
    np.save(bytestream, nparr)
    uncompressed = bytestream.getvalue()
    compressed = zlib.compress(uncompressed)
    return compressed, len(uncompressed), len(compressed)


def uncompress_nparr(bytestring):
    """
    """
    return np.load(io.BytesIO(zlib.decompress(bytestring)))


def get_random_string(length):
    letters = string.ascii_lowercase
    result_str = ''.join(random.choice(letters) for i in range(length))
    return result_str


def broadcast_receive_model():
    try:
        udp_receiver = ChunkReceiver()
    except Exception as e:
        traceback.print_exc()
        logger.error(str(e))
        udp_receiver = ChunkReceiver(port=0)
    output = udp_receiver.run(debug=True)
    logger.info(f"Receiver done, file: {output}")
    model = pickle.load(open(output, "rb"))
    logger.info(f"Receiver done, file: {output}, {model}")
    return model


def broadcast_model(model):
    filename = os.path.join(mkdtemp(), get_random_string(6) + ".pkl")
    pickle.dump(model, open(filename, "wb"))
    # torch.save(model, filename)
    logger.info(f"begin to send {filename}")
    # torch.save(model.state_dict(), filename)
    sender = ChunkSender(filename)
    sender.send()
    sender.close()
    # os.remove(filename)
