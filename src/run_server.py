# -*- coding: utf-8 -*-
import argparse
import os
import random
import math
import sys
import time
import socket
import numpy as np
import datetime

import torch
import torch.distributed as dist
from torch.utils.data import DataLoader
from flask import Flask
from flask import request

import threading
from utils import get_model_datasets, uncompress_nparr, test_model, broadcast_model
from logging_tool import logger

parser = argparse.ArgumentParser()

parser.add_argument('--data_dir', type=str, default='~/dataset/')
parser.add_argument('--data-name', type=str, default='cifar10')
parser.add_argument('--model', type=str, default='LROnMnist')
parser.add_argument('--download', type=bool, default=True)
parser.add_argument('--N', type=int, default=20, help='update epochs at server')
parser.add_argument('--inner_epoch', type=int, default=4, help='K step at each worker')
parser.add_argument('--world_size', type=int, help="world_size, start at 0")
parser.add_argument('--world_rank', type=int, help="world rank, start at 0")
parser.add_argument('--master_addr', type=str, default="", help="server addr")
parser.add_argument('--master_port', type=int, default=10086, help="server port")
parser.add_argument('--http_addr', type=str, default='0.0.0.0', help="http port")
parser.add_argument('--http_port', type=int, default=9999, help="http port")
parser.add_argument('--test_batch_size', type=int, default=400, help="http port")
parser.add_argument('--backend', type=str, default='gloo', help="http port")

flask_app = Flask(__name__)

# global
global_model = None
global_train_dataset = None
global_test_dataset = None
global_delta_w_from_workers = None
global_criterion = None
global_n = 0
lock = threading.Lock()


@flask_app.route('/upload/<int:rank>/<int:index>', methods=['POST'])
def upload(rank, index):
    """
    更新单网络层
    :param rank: worker ID
    :param index: 网络层 ID
    :return:
    """
    array = uncompress_nparr(request.data)
    tensor = torch.tensor(array)
    global_delta_w_from_workers[rank][index] = tensor
    print(f"recv param from {rank}, layer: {index}: {tensor.shape}")

    return "done"


@flask_app.route('/update/<int:rank>', methods=['GET'])
def update(rank):
    """
    更新全局权重
    :param rank:  worker ID
    :param index: 网络层 ID
    :return:
    """
    global global_n, global_criterion, global_model, global_delta_w_from_workers, global_test_dataset
    lock.acquire()
    stime = time.time()
    for idx, param in enumerate(global_model.parameters()):
        param.data -= global_delta_w_from_workers[rank][idx].clone().detach()
        # param.data -= torch.tensor(global_delta_w_from_workers[rank][idx])
    loss, acc = test_model(global_model, global_test_dataset, global_criterion)
    logger.info(f"update epoch: {global_n}, from worker:{rank}, Loss:{loss}, Acc:{acc}, Time:{time.time() - stime}")

    try:
        broadcast_model(global_model)
    except Exception as e:
        print(str(e))
        return "not broadcast"

    global_n += 1
    lock.release()
    return "done"


def start():
    global global_model, global_test_dataset, global_delta_w_from_workers, global_criterion

    world_size = args.world_size
    world_rank = args.world_rank
    hostname = socket.gethostname()
    logger.info("World_rank:{}, hostname:{}".format(world_rank, hostname))

    workers = [v for v in range(1, world_size)]

    logger.info(f"workers: {workers}")

    model_datasets = get_model_datasets(model_name=args.model, data_dir=args.data_dir, download=args.download)
    global_model = model_datasets["model"]
    test_transform = model_datasets["test_transform"]
    dataset = model_datasets["test_dataset"]
    test_bsz = 400
    global_test_dataset = DataLoader(dataset, batch_size=test_bsz, shuffle=True)

    if args.model in ['MnistCNN', 'AlexNet']:
        global_criterion = torch.nn.NLLLoss()
    else:
        global_criterion = torch.nn.CrossEntropyLoss()

    global_delta_w_from_workers = [[]] * (world_size + 1)

    for worker_index in range(world_size):
        for idx, param in enumerate(global_model.parameters()):
            global_delta_w_from_workers[worker_index].append(torch.zeros_like(param.data))
            logger.info(f"init tensor : {worker_index}, {global_delta_w_from_workers[worker_index][-1].shape}")

    server_addr_port = f'tcp://{args.master_addr}:{args.master_port}'
    logger.info(f"init_processes with  {server_addr_port}")
    os.environ['MASTER_ADDR'] = args.master_addr
    os.environ['MASTER_PORT'] = str(args.master_port)
    os.environ['GLOO_SOCKET_IFNAME'] = 'lo0'


if __name__ == '__main__':
    args = parser.parse_args()
    start()

    flask_app.run(host=args.http_addr, port=args.http_port)
