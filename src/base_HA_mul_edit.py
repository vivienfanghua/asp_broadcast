# -*- coding: utf-8 -*-
import argparse
import os
import random
import math
import sys
import time
import socket
import numpy as np

import torch
import torch.distributed as dist
from cjltest.divide_data import partition_dataset, select_dataset
from cjltest.models import MnistCNN, AlexNetForCIFAR
from cjltest.utils_data import get_data_transform
from torch.multiprocessing import Process as TorchProcess
from torch.utils.data import DataLoader
from cjltest.utils_model import MySGD
from torch.autograd import Variable
from torchvision import datasets, transforms
import threading
import ResNetOnCifar10

parser = argparse.ArgumentParser()

parser.add_argument('--data-dir', type=str, default='~/dataset/')
parser.add_argument('--data-name', type=str, default='cifar10')
parser.add_argument('--model', type=str, default='MnistCNN')
parser.add_argument('--download', type=bool, default=True)
parser.add_argument('--N', type=int, default=1, help='update epochs at server')
parser.add_argument('--inner_epoch', type=int, default=4, help='K step at each worker')


args = parser.parse_args()


def test_model(rank, model, test_data, criterion):

    test_loss = 0
    correct = 0
    model.eval()

    for data, target in test_data:
        data, target = Variable(data).cuda(dev), Variable(target).cuda(dev)
        output = model(data)
        test_loss += criterion(output, target).data.item()
        pred = output.data.max(1)[1]
        correct += pred.eq(target.data).sum().item()

    test_loss /= len(test_data)
    test_loss = format(test_loss, '.4f')
    acc = torch.tensor(correct / len(test_data.dataset))

    model.train()

    return test_loss, acc


def run_server(rank, workers, model, train_data, test_data):

    global dev, cpu
    device_count = torch.cuda.device_count()
    drank = rank % device_count
    print("torch cuda device count: {}, drank: {}".format(device_count, drank))
    dev = torch.device('cuda:{}'.format(drank))
    cpu = torch.device('cpu')

    model = model.cuda(dev)
    for param in model.parameters():
        param.data = torch.zeros_like(param.data)

    if args.model in ['MnistCNN', 'AlexNet']:
        criterion = torch.nn.NLLLoss()
    else:
        criterion = torch.nn.CrossEntropyLoss()

    # Set other variables
    N = args.N
    m = len(workers)

    # receive model params from all other workers
    # delta_w_from_workers size: worker * model_param_length
    delta_w_from_workers = [0] * (m+1)
    suc = [0] * (m+1)
    for w in workers:
        delta_w_from_workers[w] = []
        for idx, param in enumerate(model.parameters()):
            delta_w_from_workers[w].append(torch.zeros_like(param.data, device=cpu))
        # Receive tensor from w asynchronously
        # TODO flag of a worker to complete
        suc[w] = dist.irecv(delta_w_from_workers[w][0], src=w)

    n = 0
    stime = time.time()

    # Start loop
    while True:
        for w in workers:
            if suc[w].is_completed():
                print("recv param from worker : {}, {}".format(w, time.time() - stime))
                for idx in range(1, len(delta_w_from_workers[w])):
                    dist.recv(delta_w_from_workers[w][idx], src=w)
                n += 1
                for idx, param in enumerate(model.parameters()):
                    param.data -= torch.tensor(delta_w_from_workers[w][idx], device=dev)

                # Broadcast & wait for new upload
                for idx, param in enumerate(model.parameters()):
                    tensor = torch.tensor(param.data, device=cpu)
                    # broadcast to worker synchronously
                    dist.broadcast(tensor, src=0)
                suc[w] = dist.irecv(delta_w_from_workers[w][0], src=w)

                # Test the new model
                loss, acc = test_model(rank, model, test_data, criterion)
                print("idx:{}, from worker:{}, Loss:{}, Acc:{}, Time:{}".format(n, w, loss, acc, time.time()-stime))

        if n >= N:
            break


stop = False
rec = False
lock = threading.Lock()
receive_param_pipes = []


def receiver(rank, model):
    # save received params from server in pipe
    global stop, rec, receive_param_pipes, lock
    # create local params to save broadcast variable
    local_params = []
    for idx, param in enumerate(model.parameters()):
        tensor = torch.zeros_like(param.data, device=cpu)
        local_params.append(tensor)
        receive_param_pipes.append(0)

    while not stop:
        for idx in range(len(local_params)):
            dist.broadcast(local_params[idx], src=0)

        lock.acquire()
        rec = True
        for idx in range(len(receive_param_pipes)):
            receive_param_pipes[idx] = torch.tensor(local_params[idx], device=dev)
        lock.release()


def run_worker(rank, workers, model, train_data, test_data):

    global dev, cpu
    drank = rank % torch.cuda.device_count()
    dev = torch.device('cuda:{}'.format(drank))
    cpu = torch.device('cpu')

    model = model.cuda(dev)
    for param in model.parameters():
        param.data = torch.zeros_like(param.data)
    
    if args.model in ['VGG', 'MnistCNN', 'AlexNet', 'ResNet18OnCifar10']:
        optimizer = MySGD(model.parameters(), lr=0.1)
    else:
        optimizer = MySGD(model.parameters(), lr=0.01)

    if args.model in ['MnistCNN', 'AlexNet']:
        criterion = torch.nn.NLLLoss()
    else:
        criterion = torch.nn.CrossEntropyLoss()

    model.train()

    global stop, rec, receive_param_pipes, lock
    p = threading.Thread(target=receiver, args=(rank, model))
    p.start()

    print('Begin!')

    N = args.N
    WORKER_NUM = len(workers)
    INNER_EPOCH = args.inner_epoch

    computing_time_sum = 0.
    synchronization_time_sum = 0.

    accum_delta_w = []
    for _, param in enumerate(model.parameters()):
        accum_delta_w.append(torch.zeros_like(param.data))

    batch_iter = iter(train_data)
    next_upload_iter = rank * INNER_EPOCH
    n = 0

    stime = time.time()

    while True:

        n += 1
        iteration_start_time = time.time()

        # update local parameters
        try:
            data, target = next(batch_iter)
        except:
            batch_iter = iter(train_data)
            data, target = next(batch_iter)
        data, target = Variable(data).cuda(dev), Variable(target).cuda(dev)

        optimizer.zero_grad()
        output = model(data)
        loss = criterion(output, target)
        loss.backward()
        delta_ws = optimizer.get_delta_w()

        # ?
        for idx, param in enumerate(model.parameters()):
            delta_ws[idx] /= WORKER_NUM
            param.data -= delta_ws[idx]
            accum_delta_w[idx] += delta_ws[idx]

        it_computing_time = time.time()

        # Send accumulated parameters to server
        if n >= next_upload_iter:
            print("send", rank, time.time() - stime, n)
            for idx, param in enumerate(model.parameters()):
                tensor = torch.tensor(accum_delta_w[idx], device=cpu)
                dist.send(tensor, dst=0)
                accum_delta_w[idx] = torch.zeros_like(param.data)
            next_upload_iter += INNER_EPOCH * WORKER_NUM
            if rank == 1:
                print("fini", time.time() - stime, n)

        # should add this otherwise can't converge
        else:
            time.sleep(0.2)

        # Replace to new parameters if receive brocast
        if rec:
            if rank == 1:
                print(f"Rank {rank} get new param at {n}")
            lock.acquire()
            rec = False
            for idx, param in enumerate(model.parameters()):
                param.data = receive_param_pipes[idx]
            lock.release()

        it_synchronization_time = time.time()
        computing_time_sum += it_computing_time - iteration_start_time
        synchronization_time_sum += it_synchronization_time - it_computing_time

    # useless code since the process would halt within while loop
        
    # print(f'Rank {rank}, Comp avg time:{computing_time_sum/n}, Sync avg time:{synchronization_time_sum/n}, \
    #     Iteration avg time:{(computing_time_sum + synchronization_time_sum)/n}')
    
    # global stop
    # stop = True
    # p.join()


def init_processes(rank, size, workers,
                   model,
                   train_dataset, test_dataset,
                   fn, backend='mpi'):
    dist.init_process_group(backend, rank=rank, world_size=size)
    fn(rank, workers, model, train_dataset, test_dataset)


def start(world_rank, world_size):
    workers = [v for v in range(1, world_size)]

    if args.model == 'MnistCNN':
        train_transform, test_transform = get_data_transform('mnist')
        model = MnistCNN()
        train_dataset = datasets.MNIST(args.data_dir, train=True, download=args.download,
                                       transform=train_transform)
        test_dataset = datasets.MNIST(args.data_dir, train=False, download=args.download,
                                      transform=test_transform)

    elif args.model == 'AlexNet':
        train_transform, test_transform = get_data_transform('cifar')
        if args.data_name == 'cifar10':
            model = AlexNetForCIFAR()
            train_dataset = datasets.CIFAR10(args.data_dir, train=True, download=args.download,
                                             transform=train_transform)
            test_dataset = datasets.CIFAR10(args.data_dir, train=False, download=args.download,
                                            transform=test_transform)
        else:
            model = AlexNetForCIFAR(num_classes=100)
            train_dataset = datasets.CIFAR100(args.data_dir, train=True, download=args.download,
                                              transform=train_transform)
            test_dataset = datasets.CIFAR100(args.data_dir, train=False, download=args.download,
                                             transform=test_transform)

    elif args.model == 'LROnMnist':
        model = ResNetOnCifar10.LROnMnist()
        train_transform, test_transform = get_data_transform('mnist')
        train_dataset = datasets.MNIST(args.data_dir, train=True, download=args.download,
                                       transform=train_transform)
        test_dataset = datasets.MNIST(args.data_dir, train=False, download=args.download,
                                      transform=test_transform)

    elif args.model == 'LROnCifar10':
        model = ResNetOnCifar10.LROnCifar10()
        train_transform, test_transform = get_data_transform('cifar')
        train_dataset = datasets.CIFAR10(args.data_dir, train=True, download=args.download,
                                       transform=train_transform)
        test_dataset = datasets.CIFAR10(args.data_dir, train=False, download=args.download,
                                      transform=test_transform)

    elif args.model == 'ResNet18OnCifar10':
        model = ResNetOnCifar10.ResNet18()

        train_transform, test_transform = get_data_transform('cifar')
        train_dataset = datasets.CIFAR10(args.data_dir, train=True, download=args.download,
                                         transform=train_transform)
        test_dataset = datasets.CIFAR10(args.data_dir, train=False, download=args.download,
                                        transform=test_transform)
                                        
                                      
    else:
        print('Model must be {} or {}!'.format('MnistCNN', 'AlexNet'))
        sys.exit(-1)

    train_bsz = 400
    test_bsz = 400

    train_data = partition_dataset(train_dataset, workers)

    if world_rank == 0:
        train_data = 0
    else:
        train_data = select_dataset(workers, world_rank, train_data, batch_size=train_bsz)

    test_data = DataLoader(test_dataset, batch_size=test_bsz, shuffle=True)


    if world_rank == 0:
        # world_rank 0 for run_server
        p = TorchProcess(target=init_processes, args=(world_rank, world_size, workers,
                                                    model,
                                                    train_data, test_data,
                                                    run_server))
    else:
        p = TorchProcess(target=init_processes, args=(world_rank, world_size, workers,
                                                    model,
                                                    train_data, test_data,
                                                    run_worker))

    p.start()
    p.join()


if __name__ == '__main__':
    # 进程总数
    world_size = int(os.environ['OMPI_COMM_WORLD_SIZE'])
    # 进程编号
    world_rank = int(os.environ['OMPI_COMM_WORLD_RANK'])
    hostname = socket.gethostname()

    print("World_rank:{}, hostname:{}".format(world_rank, hostname))

    start(world_rank, world_size)
