docker run --name master -m 2G -d --net=mynet -p 9999:9999 -v /Users/huaqiang.fhq/code/lab/docker/src:/code/ -it python36
docker run --name worker1 -m 2G -d --net=mynet -p 5845:5845 -v /Users/huaqiang.fhq/code/lab/docker/src:/code/ -it python36
docker run --name worker2 -m 2G -d --net=mynet -p 5846:5845 -v /Users/huaqiang.fhq/code/lab/docker/src:/code/ -it python36


docker run --name python1 --net=bridge -p 8265:8265 -p 9999:9999 -d -v /Users/huaqiang.fhq/code/lab/docker/src:/code/ -it my-torch-mpi-image
docker run --name python1 --net=mynet --ip 172.65.12.1 -d -v /Users/huaqiang.fhq/code/lab/docker/src:/code/ --privileged  /usr/sbin/init -it python36_ssh

docker cp src/base_HA_multinodes.py python-1:/home/admin/
docker cp src/base_HA_multinodes.py python-2:/home/admin/

docker run -d --privileged --name ssh.1 --env "SSH_PASSWORD_AUTHENTICATION=true" --env "SSH_USER=admin" --env "SSH_USER_PASSWORD=admin" -p 2020:22 jdeathe/centos-ssh:2.6.1

docker exec -it python-1 python3 /code/run_server.py --world_size 3 --world_rank 0 --master_addr 172.17.0.2 --master_port 8265 --download False --data_dir /home/admin/data/
docker exec -it python-2 python3 /code/run_worker.py --world_size 3 --world_rank 1 --master_addr 172.17.0.2 --master_port 8265 --download False --data_dir /home/admin/data/

export BASEDIR=/home/admin/docker
python3 $BASEDIR/src/run_server.py --world_size 3 --world_rank 0 --http_addr 0.0.0.0 --master_port 9999 --download False --data_dir $BASEDIR/data

python3 $BASEDIR/src/run_worker.py --world_size 3 --world_rank 1 --http_addr 172.65.0.1 --http_port 9999 --download False --data_dir $BASEDIR/data

python3 $BASEDIR/src/run_worker.py --world_size 3 --world_rank 2 --http_addr 172.65.0.1 --http_port 9999 --download False --data_dir $BASEDIR/data


docker run --name python3 --net=host -d -v /Users/huaqiang.fhq/code/lab/docker/src:/code/ -it my-torch-mpi-image
docker exec -it python-1 python3 /code/base_HA_multinodes.py --world_size 2 --world_rank 0 --master_addr 172.17.0.2 --master_port 8265 --download False --data-dir /home/admin/data/


